<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Laravel\Passport\Client;

class InstallApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'teams:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This installs a fresh copy of the Teams App';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->call('migrate:fresh');
		$this->call('db:seed');
		$this->call('passport:client',['--password' => true]);
		$this->call('passport:client',['--personal' => true]);

		$this->replaceApiSecret();
	}

	private function replaceApiSecret()
	{
		$key = Client::where('password_client', '1')->first()->secret;

		$escaped = preg_quote('=' . $this->laravel['config']['app.api_credentials.API_CLIENT_SECRET'], '/');

		file_put_contents($this->laravel->environmentFilePath(), preg_replace(
			"/^API_CLIENT_SECRET{$escaped}/m",
			'API_CLIENT_SECRET=' . $key,
			file_get_contents($this->laravel->environmentFilePath())
		));

	}
}
