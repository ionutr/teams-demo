<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name',
		'last_name',
		'age'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'user_id',
		'team_id'
	];

	public function author()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function team()
	{
		return $this->belongsTo('App\Team', 'team_id', 'id');
	}
}
