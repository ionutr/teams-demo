<?php

namespace App\Http\Controllers;

class AppController extends Controller
{
	/**
	 * Main entry point - routes to vue-router
	 */
    public function index()
	{
		return view('app');
	}
}
