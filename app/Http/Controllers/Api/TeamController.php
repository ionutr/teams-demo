<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\TeamRequest;
use App\Http\Resources\TeamCollectionResource;
use App\Http\Resources\TeamResource;
use App\Team;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
	/**
	 * Get team list
	 *
	 * @param  [TeamRequest] $request
	 * @return [TeamCollectionResource]
	 */
	public function index(TeamRequest $request)
	{
		return response()->json(
			new TeamCollectionResource(
				Team::all()
					->sortBy('name', SORT_REGULAR, false)
					->load('author')
			)
		);
	}

	/**
	 * Create a team
	 *
	 * @param  [TeamRequest] $request
	 * @return [TeamResource]
	 */
    public function create(TeamRequest $request)
	{
		return response()->json(new TeamResource(
			factory(Team::class)->create(array_merge(
					$request->all(),
					[
						'user_id' => auth()->id()
					]
				)
			)
		), 201);
	}

	/**
	 * Get a team
	 *
	 * @param  [TeamRequest] $request
	 * @param  [Team] $team
	 * @return [TeamResource]
	 */
	public function view(TeamRequest $request, Team $team)
	{
		return response()->json(new TeamResource($team->load('players', 'players.author')));
	}
}
