<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\PlayerRequest;
use App\Http\Resources\PlayerResource;
use App\Player;
use App\Http\Controllers\Controller;

class PlayerController extends Controller
{
	/**
	 * Create a player
	 *
	 * @param  [PlayerRequest] $request
	 * @return [PlayerResource]
	 */
	public function create(PlayerRequest $request)
	{
		return response()->json(new PlayerResource(
			factory(Player::class)->create(array_merge(
					$request->all(),
					[
						'user_id' => auth()->id()
					]
				)
			)
		), 201);
	}

	/**
	 * Update a player
	 *
	 * @param  [PlayerRequest] $request
	 * @param  [Player] $player
	 * @return [PlayerResource]
	 */
	public function update(PlayerRequest $request, Player $player)
	{
		$player->update($request->all());

		return response()->json(new PlayerResource($player));
	}
}
