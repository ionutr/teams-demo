<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class PlayerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
		return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return ($request->getMethod() === 'POST') ? [
        	'team_id' => 'numeric|required|exists:teams,id',
            'first_name' => 'string|required',
            'last_name' => 'string|required',
			'age' => 'numeric|required'
        ] : [
			'first_name' => 'string|required',
			'last_name' => 'string|required',
			'age' => 'numeric|required'
		];
    }
}
