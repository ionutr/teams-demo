<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'user_id'
	];

	public function author()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function players()
	{
		return $this->hasMany('App\Player');
	}
}
