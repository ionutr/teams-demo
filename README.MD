# Teams APP Demo

## Prerequisites
* php7+
* composer
* npm
* MySQL

## Installation

1. Clone the git repository: ```git clone https://ionutr@bitbucket.org/ionutr/teams-demo.git```
2. Switch to the directory where you cloned the repository(**teams-demo**). 
3. Run ```composer install```
4. Copy **.env.example** to **.env** 
5. Run ```php artisan key:generate```
6. Generate passport keys: ```php artisan passport:keys```
6. Create a MySQL database and grant access to it to a user
7. Edit the **.env** file with the MySQL connection data
8. Install the app: ```php artisan teams:install```
9. Install the frontend dependencies: ```npm install```
10. Run artisan server: ```php artisan serve```
11. Open a browser window and go to [http://localhost:8000](http://localhost:8000)