
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import 'bootstrap/dist/css/bootstrap.css'
import 'vue-snotify/styles/material.scss'
import 'vue-loading-overlay/dist/vue-loading.css'

window.Vue = require('vue');

import Loading from 'vue-loading-overlay'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

window.Vue.use(Loading, {
	color: '#666',
	isFullPage: true,
	loader: 'dots'
})
window.Vue.use(Snotify, {
	toast: {
		position: SnotifyPosition.rightTop,
		timeout: 5000
	}
})
window.Vue.use(VueRouter)
window.Vue.use(Vuex)

import App from './components/App.vue'
import LoginPage from './components/pages/LoginPage.vue'
import TeamPage from './components/pages/TeamPage.vue'
import TeamListPage from './components/pages/TeamListPage.vue'

const routes = [
	{
		path: '/',
		component: TeamListPage,
	},
	{
		name: 'route-teampage',
		path: '/teams/:id',
		component: TeamPage,
	},
    {
        path: '/login',
        component: LoginPage
    }
]

const router = new VueRouter({
	name: 'router',
	mode: 'history',
	routes: routes
})

const API_URL = 'http://localhost:8000/api/'
const _TOKEN = 'teams-token'

axios.defaults.baseURL = API_URL
axios.defaults.xsrfCookieName = false
delete axios.defaults.headers.common['X-CSRF-TOKEN']
delete axios.defaults.headers.common['X-XSRF-TOKEN']

const storedToken = localStorage.getItem(_TOKEN)
if (storedToken) {
	axios.defaults.headers.common['Authorization'] = 'Bearer ' + storedToken
}

const store = new Vuex.Store({
	strict: true,
	state: {
		accessToken: localStorage.getItem(_TOKEN) || '',
		errors: [],
		messages: [],
		status: 'idle',
		user: null
	},
	mutations: {
		APP_ERROR(state, error) {
			state.errors.push(error)
		},
		APP_ERRORS(state, errors) {
			for (var errorKey in errors) {
				errors[errorKey].forEach((error) => {
					state.errors.push(error)
				})
			}
		},
		APP_MESSAGE(state, message) {
			state.messages = []
			state.messages.push(message)
		},
		AUTH_LOGOUT(state) {
			state.accessToken = false
			state.user = null
		},
		AUTH_SUCCESS(state, token, user) {
			state.status = 'success'
			state.accessToken = token
			state.user = user
		},
		ERROR_RESET(state) {
			state.errors = []
		},
		REQUEST_INIT(state) {
			state.status = 'loading'
		},
		REQUEST_STOP(state) {
			state.status = 'idle'
		}
	},
	actions: {
		AUTH_REQUEST: ({commit}, credentials) => {
			return new Promise((resolve, reject) => {
				commit('REQUEST_INIT')
				commit('ERROR_RESET')

				axios.post(`${API_URL}login`, credentials).then((response) => {
					const token = response.data.access_token,
						user = response.data.user

					localStorage.setItem(_TOKEN, token)
					axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
					commit('AUTH_SUCCESS', token, user)
					commit('APP_MESSAGE', 'Successfully logged in')
					commit('REQUEST_STOP')
					resolve(response)
				}).catch((error) => {
					commit('APP_ERROR', error.response.data.message)
					localStorage.removeItem(_TOKEN)
					delete axios.defaults.headers.common['Authorization']
					commit('REQUEST_STOP')
					reject(error)
				})
			})
		},
		LOGOUT_REQUEST: ({commit}) => {
			return new Promise((resolve, reject) => {
				commit('REQUEST_INIT')
				axios.get(`${API_URL}logout`).then(() => {
					commit('AUTH_LOGOUT')
					commit('APP_MESSAGE', 'Successfully logged out')
					localStorage.removeItem(_TOKEN)
					delete axios.defaults.headers.common['Authorization']
					commit('REQUEST_STOP')
					resolve()
				})
			})
		}
	},
	getters: {
		isAuthenticated: state => !!state.accessToken
	}
})

axios.interceptors.response.use(undefined, (err) => {
	return new Promise((commit) => {
		if (err.response.status === 401) {
			commit('AUTH_LOGOUT')
			commit('APP_MESSAGE', 'Unauthenticated, redirecting to login')
			router.push('/login')
			commit('REQUEST_STOP')
		}

		throw err
	})
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
	router,
	store,
	render: h => h(App)
});
