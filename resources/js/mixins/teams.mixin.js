export default {
	methods: {
		createTeam(data) {
			let request = window.axios.post('teams', data, {
				withCredentials: true
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		},
		getTeam(id) {
			let request = window.axios.get(`teams/${id}`, {
				withCredentials: true
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		},
		getTeams() {
			let request = window.axios.get('teams', {
				withCredentials: true
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		}
	},
	name: 'TeamsMixin'
}