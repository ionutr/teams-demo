export default {
	methods: {
		createPlayer(data) {
			let request = window.axios.post('players', data, {
				withCredentials: true
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		},
		updatePlayer(id, data) {
			let request = window.axios.put(`players/${id}`, data, {
				withCredentials: true
			})

			return request
				.then(result => { return result })
				.catch(error => { throw error })
		}
	},
	name: 'PlayersMixin'
}