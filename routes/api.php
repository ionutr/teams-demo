<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
	'middleware' => ['guest']
], function() {
	Route::post('login', 'Api\AuthController@login');
});

Route::group([
	'middleware' => ['auth:api']
], function() {
	Route::get('teams', 'Api\TeamController@index');
	Route::post('teams', 'Api\TeamController@create');
	Route::get('teams/{team}', 'Api\TeamController@view');

	Route::post('players', 'Api\PlayerController@create');
	Route::put('players/{player}', 'Api\PlayerController@update');

	Route::get('logout', 'Api\AuthController@logout');
});