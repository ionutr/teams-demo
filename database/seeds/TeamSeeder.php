<?php

use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Seed with teams
     *
     * @return void
     */
    public function run()
    {
    	$teamNames = [
			'FC Barcelona',
			'FC Real Madrid',
			'AC Milan',
			'FC Bayern München',
		];

		foreach ($teamNames as $teamName) {
			factory(App\Team::class)->create([
				'name' => $teamName
			]);
    	}
    }
}
