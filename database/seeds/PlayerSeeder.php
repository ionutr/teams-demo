<?php

use App\Team;
use Illuminate\Database\Seeder;

class PlayerSeeder extends Seeder
{
    /**
     * Seed with some players
     *
     * @return void
     */
    public function run()
    {
    	$barcelona = Team::whereName('FC Barcelona')->first();
    	$realMadrid = Team::whereName('FC Real Madrid')->first();

		factory(App\Player::class)->create([
			'team_id' => $barcelona->id,
			'first_name' => 'Leo',
			'last_name' => 'Messi',
			'age' => 30
		]);

		factory(App\Player::class)->create([
			'team_id' => $barcelona->id,
			'first_name' => 'Philippe',
			'last_name' => 'Coutinho',
			'age' => 26
		]);

		factory(App\Player::class)->create([
			'team_id' => $realMadrid->id,
			'first_name' => 'Gareth',
			'last_name' => 'Bale',
			'age' => 29
		]);
    }
}
