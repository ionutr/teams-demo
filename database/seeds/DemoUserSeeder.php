<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DemoUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(App\User::class)->create([
			'name' => 'Demo User',
			'email' => 'demo@teams.org',
			'password' => Hash::make('1234')
		]);
    }
}
